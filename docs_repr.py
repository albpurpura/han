import nlp
import util
from time import time
from sklearn.feature_extraction.text import TfidfVectorizer
import os.path
from tqdm import tqdm


def get_tf_idf_repr(docs, n_features, lang='english', min_df=2, max_df=0.95, verbose=False):
    tfidf_vectorizer = TfidfVectorizer(max_df=max_df, min_df=min_df,
                                       max_features=n_features,
                                       stop_words=lang, tokenizer=nlp.tokenize)
    t0 = time()
    tfidf = tfidf_vectorizer.fit_transform(docs)
    tfidf_feature_terms = tfidf_vectorizer.get_feature_names()
    if verbose:
        print("done in %0.3fs." % (time() - t0))
        print('number of documents: ' + str(len(docs)))
        print('number of features: ' + str(len(tfidf_feature_terms)))

    # return tfidf.todense()
    return tfidf, tfidf_feature_terms


def get_docs(input_folder):
    filenames = []
    docs_text = []

    for filename in os.listdir(input_folder):
        if filename.startswith('.') or os.path.isdir(os.path.join(input_folder, filename)):
            continue
        with open(os.path.join(input_folder, filename), 'r', encoding='latin-1') as f:  # Reading file
            content = ''
            for l in f:
                content += l
            content.strip().lower()
            docs_text.append(content)
            filenames.append(filename.split('.')[0])
    return filenames, docs_text


def get_20news_docs(main_folder):
    subfolders = [f.path for f in os.scandir(main_folder) if f.is_dir()]
    documents_all_text = []
    labels = []
    label = 0
    for sf in subfolders:
        filenames, docs_text = nlp.get_docs(sf)
        for d in docs_text:
            documents_all_text.append(d)
            labels.append(label)
        label += 1
    return documents_all_text, labels
