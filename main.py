import tensorflow as tf
import numpy as np
import time
import docs_repr as dr
from sklearn.metrics import accuracy_score
from tqdm import tqdm

import nlp
import os
from gensim.models.keyedvectors import KeyedVectors
from sklearn.model_selection import train_test_split


class HAN_autoencoder:
    @staticmethod
    def get_encoder(fw_cell, bw_cell, inputs_embedded, word_level_lengths):
        # inputs: Tensor of shape [batch_size, max_time, input_size]. max_time = number of words in a sequence.
        # outputs: Tuple (output_fw, output_bw) containing the forward and the backward rnn output Tensor.

        outputs_encoder, output_states = tf.nn.bidirectional_dynamic_rnn(cell_fw=fw_cell, cell_bw=bw_cell,
                                                                         inputs=inputs_embedded,
                                                                         sequence_length=word_level_lengths,
                                                                         dtype=tf.float32)
        (fw_outputs, bw_outputs) = outputs_encoder
        outputs = tf.concat((fw_outputs, bw_outputs), 2)
        return outputs

    @staticmethod
    def get_attention(inputs, output_size, initializer=tf.contrib.layers.xavier_initializer(), activation_fn=tf.tanh):
        # inputs: Tensor of shape [batch_size, units, input_size]
        # outputs: Tensor of shape [batch_size, output_dim].
        attention_context_vector = tf.get_variable(name='attention_context_vector',
                                                   shape=[output_size],
                                                   initializer=initializer,
                                                   dtype=tf.float32)

        projection = tf.contrib.layers.fully_connected(inputs, output_size, activation_fn=activation_fn)
        vector_attn = tf.reduce_sum(tf.multiply(projection, attention_context_vector), axis=2, keep_dims=True)
        attention_weights_words = tf.nn.softmax(vector_attn, dim=1)
        weighted_projections = tf.multiply(projection, attention_weights_words)
        output_attention = tf.reduce_sum(weighted_projections, axis=1)
        return output_attention

    def __init__(self, word_embeddings_matrix, vocab_size, embedding_size, n_classes, word_output_size=100,
                 sentence_output_size=100, learning_rate=1e-4, cell_dim=50, max_grad_norm=5.0):
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.word_output_size = word_output_size
        self.sentence_output_size = sentence_output_size
        self.n_classes = n_classes
        self.cell_dim = cell_dim
        self.word_embeddings_matrix = word_embeddings_matrix
        self.max_grad_norm = max_grad_norm

        self.global_step = tf.Variable(0, name='global_step', trainable=False)

        # [document x sentence x word]
        self.inputs = tf.placeholder(shape=(None, None, None), dtype=tf.int32, name='inputs')
        # [document x sentence]
        self.word_lengths = tf.placeholder(shape=(None, None), dtype=tf.int32, name='word_lengths')
        # [document]
        self.sentence_lengths = tf.placeholder(shape=(None,), dtype=tf.int32, name='sentence_lengths')
        # [document]
        self.labels = tf.placeholder(shape=(None,), dtype=tf.int32, name='labels')
        (self.document_size, self.sentence_size, self.word_size) = tf.unstack(tf.shape(self.inputs))

        # initialize word embeddings lookup
        self.embedding_matrix = tf.constant(self.word_embeddings_matrix, name='embedding_matrix')
        self.inputs_embedded = tf.nn.embedding_lookup(self.embedding_matrix, self.inputs)

        # initialize network components
        self._init_body()

        # training operations
        self.cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.labels, logits=self.logits)
        self.loss = tf.reduce_mean(self.cross_entropy)
        tf.summary.scalar('loss', self.loss)

        self.accuracy = tf.reduce_mean(tf.cast(tf.nn.in_top_k(self.logits, self.labels, 1), tf.float32))
        tf.summary.scalar('accuracy', self.accuracy)

        tvars = tf.trainable_variables()

        grads, global_norm = tf.clip_by_global_norm(tf.gradients(self.loss, tvars), self.max_grad_norm)
        tf.summary.scalar('global_grad_norm', global_norm)

        opt = tf.train.AdamOptimizer(learning_rate)

        self.train_op = opt.apply_gradients(zip(grads, tvars), name='train_op', global_step=self.global_step)

        self.summary_op = tf.summary.merge_all()

    def get_feed_data(self, x, y):
        x_m, doc_sizes, sent_sizes = batch(x)
        fd = {self.inputs: x_m, self.sentence_lengths: doc_sizes, self.word_lengths: sent_sizes, self.labels: y}
        return fd

    def _init_body(self):
        word_level_inputs = tf.reshape(self.inputs_embedded, [self.document_size * self.sentence_size,
                                                              self.word_size,
                                                              self.embedding_size
                                                              ])

        word_level_lengths = tf.reshape(self.word_lengths, [self.document_size * self.sentence_size])
        with tf.variable_scope('word') as scope:
            fw_cell_w = tf.contrib.rnn.GRUCell(self.cell_dim)
            bw_cell_w = tf.contrib.rnn.GRUCell(self.cell_dim)
            # Word encoder
            outputs_w_encoder = self.get_encoder(fw_cell_w, bw_cell_w, word_level_inputs,
                                                 word_level_lengths)
            # Word attention
            word_level_output = self.get_attention(outputs_w_encoder, self.word_output_size)

            sentence_inputs = tf.reshape(word_level_output,
                                         [self.document_size, self.sentence_size, self.word_output_size])
        with tf.variable_scope('sentence') as scope:
            fw_cell_s = tf.contrib.rnn.GRUCell(self.cell_dim)
            bw_cell_s = tf.contrib.rnn.GRUCell(self.cell_dim)
            # Sentence encoder
            outputs_s_encoder = self.get_encoder(fw_cell_s, bw_cell_s, sentence_inputs,
                                                 self.sentence_lengths)
            # Sentence attention
            sentence_level_output = self.get_attention(outputs_s_encoder, self.sentence_output_size)

        # Document classification
        self.logits = tf.contrib.layers.fully_connected(sentence_level_output, self.n_classes, activation_fn=None)
        self.predictions = tf.argmax(self.logits, axis=-1)


def get_embeddings_matrix(word_index, embs_path='models/GoogleNews-vectors-negative300.bin', emb_size=300):
    vocab_size = len(word_index.keys())
    model = KeyedVectors.load_word2vec_format(embs_path, binary=True)
    embeddings = np.zeros((vocab_size + 1, emb_size))

    for k, v in word_index.items():
        if k in model.wv.vocab:
            embeddings[v] = model[k]
        else:
            embeddings[v] = np.zeros(emb_size)  # unk words are mapped to all zero vector

    return np.array(embeddings, dtype=np.float32), vocab_size, emb_size


def batch_iterator(x_train, y_train, batch_size=100, max_epochs=10):
    xb = []
    yb = []
    for i in range(max_epochs):
        for j in range(len(x_train)):
            x = x_train[j]
            y = y_train[j]
            xb.append(x)
            yb.append(y)
            if len(xb) == batch_size:
                yield xb, yb
                xb, yb = [], []


def batch(inputs):
    batch_size = len(inputs)

    document_sizes = np.array([len(doc) for doc in inputs], dtype=np.int32)
    document_size = document_sizes.max()

    sentence_sizes_ = [[len(sent) for sent in doc] for doc in inputs]
    sentence_size = max(map(max, sentence_sizes_))

    b = np.zeros(shape=[batch_size, document_size, sentence_size], dtype=np.int32)  # == PAD

    sentence_sizes = np.zeros(shape=[batch_size, document_size], dtype=np.int32)
    for i, document in enumerate(inputs):
        if i == batch_size:
            break
        for j, sentence in enumerate(document):
            sentence_sizes[i, j] = sentence_sizes_[i][j]
            for k, word in enumerate(sentence):
                b[i, j, k] = word

    return b, document_sizes, sentence_sizes


def run():
    main_folder = 'data/20news-bydate/20news-bydate-train'
    if not os.path.isfile('models/labels') or not os.path.isfile('models/docs'):
        docs, labels = dr.get_20news_docs(main_folder)
        nlp.save_model(labels, 'models/labels')
        nlp.save_model(docs, 'models/docs')
    else:
        labels = nlp.load_model('models/labels')
        docs = nlp.load_model('models/docs')

    n_classes = max(labels) + 1

    if not os.path.isfile('models/word_embeddings_matrix') or not os.path.isfile('models/encoded_collection'):
        encoded_collection, word_index = nlp.encode_doc_collection(docs)
        word_embeddings_matrix, vocab_size, embedding_size = get_embeddings_matrix(word_index)
        nlp.save_model(word_embeddings_matrix, 'models/word_embeddings_matrix')
        nlp.save_model(vocab_size, 'models/vocab_size')
        nlp.save_model(embedding_size, 'models/embedding_size')
        nlp.save_model(encoded_collection, 'models/encoded_collection')
        nlp.save_model(word_index, 'models/word_index')
    else:
        encoded_collection = nlp.load_model('models/encoded_collection')
        word_index = nlp.load_model('models/word_index')
        word_embeddings_matrix = nlp.load_model('models/word_embeddings_matrix')
        vocab_size = nlp.load_model('models/vocab_size')
        embedding_size = nlp.load_model('models/embedding_size')

    x_train, x_test, y_train, y_test = train_test_split(encoded_collection, labels, test_size=0.3, random_state=42)
    _, x_val, _, y_val = train_test_split(x_test, y_test, test_size=0.01, random_state=42)

    tf.reset_default_graph()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as session:
        model = HAN_autoencoder(word_embeddings_matrix, vocab_size, embedding_size, n_classes)
        saver = tf.train.Saver(tf.global_variables())
        checkpoint_name = 'autoencoder' + '-model'
        checkpoint_dir = 'checkpoints'
        checkpoint_path = os.path.join(checkpoint_dir, checkpoint_name)
        checkpoint = tf.train.get_checkpoint_state(checkpoint_dir)
        if checkpoint:
            print("Reading model parameters from %s" % checkpoint.model_checkpoint_path)
            saver.restore(session, checkpoint.model_checkpoint_path)
        else:
            print("Created model with fresh parameters")
            session.run(tf.global_variables_initializer())

        # test(session, model, x_test, y_test)
        # exit(0)

        train(session, model, saver, checkpoint_path, 'log', x_train, y_train, x_val, y_val)


def test(session, model, x_test, y_test):
    y_pred = []
    for i in tqdm(range(len(x_test))):
        x = [x_test[i]]
        y = [y_test[i]]
        fd = model.get_feed_data(x, y)
        y_pred.append(session.run([model.predictions], fd)[0][0])
    acc = accuracy_score(y_test, y_pred, normalize=True)
    print('accuracy:%s' % acc)
    return acc, y_pred


def train(session, model, saver, checkpoint_path, tflog_dir, x_train, y_train, x_val, y_val):
    validation_accuracies = []
    checkpoint_frequency = 30000
    eval_frequency = 10
    summary_writer = tf.summary.FileWriter(tflog_dir, graph=tf.get_default_graph())
    batch_size = 10
    try:
        for i, (x, y) in enumerate(batch_iterator(x_train, y_train, batch_size=batch_size)):
            fd = model.get_feed_data(x, y)
            t0 = time.clock()
            step, summaries, loss, accuracy, _ = session.run([
                model.global_step,
                model.summary_op,
                model.loss,
                model.accuracy,
                model.train_op,
            ], fd)
            td = time.clock() - t0
            summary_writer.add_summary(summaries, global_step=step)

            if step % 1 == 0:
                print('step %s, loss=%s, accuracy=%s, t=%s, inputs=%s' % (
                    step, loss, accuracy, round(td, 2), fd[model.inputs].shape))
            if step != 0 and step % checkpoint_frequency == 0:
                print('checkpoint & graph meta')
                saver.save(session, checkpoint_path, global_step=step)
                print('checkpoint done')

            if step % eval_frequency == 0:
                print('Evaluating on validation set: ')
                acc = session.run([model.accuracy], feed_dict=model.get_feed_data(x_val, y_val))
                print('validation accuracy: %s' % acc)
                validation_accuracies.append(acc)
        print('finished training, saving session:')
    finally:
        saver.save(session, checkpoint_path, global_step=model.global_step)
        print('saving validation set accuracies to file')
        out = open('validation_accuracies_step_' + str(model.global_step.eval()) + '.txt', 'w')
        for v in validation_accuracies:
            out.write(str(v) + '\n')
        out.close()


if __name__ == '__main__':
    run()
