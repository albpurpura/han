import collections
import os
import string
import pickle

from gensim.corpora import Dictionary
from nltk import sent_tokenize, re, PorterStemmer, word_tokenize
from nltk.corpus import stopwords
from tqdm import tqdm
from keras.preprocessing.text import Tokenizer


def sentence_tokenize(documents):
    sent_tokenized_docs = []
    for d in tqdm(documents):
        sent_tokenized_docs.append(sent_tokenize(d))
    return sent_tokenized_docs


def get_docs(input_folder, encoding='latin-1'):
    filenames = []
    docs_text = []
    for filename in os.listdir(input_folder):
        if filename.startswith('.') or os.path.isdir(os.path.join(input_folder, filename)):
            continue
        with open(os.path.join(input_folder, filename), 'r', encoding=encoding) as f:  # Reading file
            content = ''
            for l in f:
                content += l
            content.strip().lower()
            docs_text.append(content)
            filenames.append(filename.split('.')[0])
    return filenames, docs_text


def tokenize(text, stemming=False, remove_stopwords=True):
    """
    ONLY WORKS FOR ENGLISH (stemming + stopwords removal)
    Tokenizes a text removing first all punctuation characters and then splittin the text on space.
    :param remove_stopwords:
    :param stemming:
    :param text:
    :return: the list of tokens in the text
    """
    # text = text.lower()
    # translator = re.compile('[%s]' % re.escape(string.punctuation))  # will be used to remove punctuation
    # line = translator.sub(' ', text)  # replace punctuation with space
    # tokens = line.replace('  ', ' ').split()
    stops = stopwords.words('english') + list(string.punctuation)
    tokens = [word for word in word_tokenize(text.lower()) if word not in stops]
    if stemming:
        stemmer = PorterStemmer()
        tokens = [stemmer.stem(t) for t in tokens]
    return tokens


def untokenize(document):
    return ' '.join(document)


def encode_doc_collection(documents):
    vocab_size = 0
    encoded_collection = []
    word_index = {}
    print('Splitting documents in sentences')
    documents = sentence_tokenize(documents)
    print('Encoding documents')
    for d in tqdm(documents):
        encoded_doc = []
        for s in d:
            encoded_sentence = []
            tokens = tokenize(s, stemming=False, remove_stopwords=True)
            for t in tokens:
                if t not in word_index.keys():
                    vocab_size += 1
                    word_index[t] = vocab_size
                encoded_sentence.append(word_index[t])
            if len(encoded_sentence) > 1:
                encoded_doc.append(encoded_sentence)
        encoded_collection.append(encoded_doc)
    return encoded_collection, word_index


def encode_collection_keras(documents, max_n_terms=60000):
    print('pre-processing documents')
    new_d = []
    for d in tqdm(documents):
        new_d.append(untokenize(tokenize(d)))
    documents = new_d
    # documents = [untokenize(tokenize(d)) for d in documents]
    print('indexing documents')
    tokenizer = Tokenizer(num_words=max_n_terms)
    tokenizer.fit_on_texts(documents)

    sequences = tokenizer.texts_to_sequences(documents)
    return sequences, tokenizer.word_index


def build_word_dictionary(docs, min_freq=1, max_word_num=60000):
    """create word dictionary"""
    # turn documents into list of words
    tokenized_docs = tokenize(docs, remove_stopwords=True)
    words_in_collection = [word for doc in tokenized_docs for word in doc]
    # initialize list of [word, word_count] for each word starting with UNKNOWN
    count = [('UNK', -1)]
    # add words and sort them by count
    count.extend([(word, count) for word, count in collections.Counter(words_in_collection).most_common(max_word_num)
                  if count >= min_freq])
    # create the dictionary
    word_dict = dict()
    # for each word - that we want in the dictionary - add it and make it the value of the prior dictionary length
    for word, word_count in count:
        word_dict[word] = len(word_dict)
    return word_dict


def save_model(model, output_path):
    with open(output_path, 'wb') as handle:
        pickle.dump(model, handle)
        handle.close()


def load_model(path):
    model = pickle.load(open(path, 'rb'))
    return model
