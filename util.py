import pickle
import subprocess
import os
import json

import nlp
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from gensim.models import KeyedVectors
from sklearn.decomposition import PCA
from ggplot import *


def save_json(model, output_path):
    with open(output_path, 'w') as outfile:
        json.dump(model, outfile)


def load_json(path):
    with open(path, 'r') as json_file:
        return json.load(json_file)


def save_model(model, output_path):
    with open(output_path, 'wb') as handle:
        pickle.dump(model, handle)
        handle.close()


def load_model(path):
    model = pickle.load(open(path, 'rb'))
    return model


def run_trec_eval(trec_eval_path='trec_eval.8.1/trec_eval',
                  q_rels_file='data/trec/qrels.robust2004.complete.txt',
                  run_to_eval='/Users/albertopurpura/PycharmProjects/ml4ir_git/results/re_ranking_output_lmnn.txt'):
    command = os.path.join(os.getcwd(), trec_eval_path) + ' ' \
              + os.path.join(os.getcwd(), q_rels_file) + ' ' \
              + os.path.join(os.getcwd(), run_to_eval) + ' | grep "^map" '
    (map_line, err) = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()
    map_line = map_line.decode("utf-8")
    map_value = map_line.split('\t')[2]
    return float(map_value)


def chunck(l, size):
    chnks = []
    for i in range(0, len(l), size):
        chnks.append(l[i: min(i + size, len(l))])
    return chnks


def get_embeddings_matrix(word_index, embs_path='models/GoogleNews-vectors-negative300.bin', emb_size=300):
    vocab_size = len(word_index.keys())
    model = KeyedVectors.load_word2vec_format(embs_path, binary=True)
    embeddings = np.zeros((vocab_size + 1, emb_size))

    for k, v in word_index.items():
        if k in model.wv.vocab:
            embeddings[v] = model[k]
        else:
            embeddings[v] = np.zeros(emb_size)  # unk words are mapped to all zero vector

    return np.array(embeddings, dtype=np.float32), vocab_size, emb_size


def encode_collection(collection, models_folder, max_features_num=60000, collection_name='collection'):
    if not os.path.isfile(models_folder + '/word_embeddings_matrix_' + collection_name):
        encoded_collection, word_index = nlp.encode_collection_keras(collection, max_features_num)
        word_embeddings_matrix, vocab_size, embedding_size = get_embeddings_matrix(word_index)
        nlp.save_model(word_embeddings_matrix, models_folder + '/word_embeddings_matrix_' + collection_name)
        nlp.save_model(encoded_collection, models_folder + '/encoded_collection_' + collection_name)
        nlp.save_model(word_index, models_folder + '/word_index_' + collection_name)
    else:
        encoded_collection = nlp.load_model(models_folder + '/encoded_collection_'
                                                            '' + collection_name)
        word_embeddings_matrix = nlp.load_model(models_folder + '/word_embeddings_matrix_' + collection_name)
        word_index = nlp.load_model(models_folder + '/word_index_' + collection_name)

    return encoded_collection, word_embeddings_matrix


def split_doc_in_frames(collection, max_sentence_size=20, max_sentences_per_doc=None):
    fixed_sizes_collections = []
    for i in tqdm(range(len(collection))):
        d = collection[i]
        # a document is a list of sentences
        max_sentence_size_in_words = max_sentence_size
        frames = chunck(d, max_sentence_size_in_words)
        if max_sentences_per_doc is not None:
            frames = frames[:max_sentences_per_doc]
        fixed_sizes_collections.append(frames)
    return fixed_sizes_collections


def plot_de(doc_e, labels, step):
    pca = PCA(n_components=2)
    pca_result = pca.fit_transform(doc_e)

    x = pca_result[:, 0]
    y = pca_result[:, 1]
    df = pd.DataFrame()
    df['pca-one'] = x
    df['pca-two'] = y
    df['label'] = labels
    chart = ggplot(df, aes(x='pca-one', y='pca-two', color='label')) \
            + geom_point(size=75, alpha=0.8) \
            + ggtitle("First and Second Principal Components colored by digit")
    chart.save('chart_step' + str(step) + '.png')
